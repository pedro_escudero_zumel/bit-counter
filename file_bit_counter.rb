class FileBitCounter

  def self.count_bits(file)
    File.exists?(file) ? perform_bit_count(file) : puts("File #{file} doesn't exists")
  end

  private

  def self.perform_bit_count(file)
    zero_counter = one_counter = 0 
    File.read(file).each_char do |char|
      char.ord.to_s(2).tap{|numbers| zero_counter += numbers.count('0'); one_counter += numbers.count('1') }
    end
    "Zeros: #{zero_counter}, Ones: #{one_counter}"
  end

end