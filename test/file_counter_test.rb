require_relative "../file_bit_counter.rb"

require "minitest/autorun"

class TestFileCounter < Minitest::Test

  def test_file_doesnt_exists
    assert_equal(FileBitCounter.count_bits('rtt.rb'), nil)
  end

  def test_simple_file
    assert_equal(FileBitCounter.count_bits('test/simple.txt'), "Zeros: 5, Ones: 2")
  end

  def test_complex_file
    assert_equal(FileBitCounter.count_bits('test/complex.txt'), "Zeros: 7069, Ones: 8505")
  end

  def test_empty_file
    assert_equal(FileBitCounter.count_bits('test/empty.txt'), "Zeros: 0, Ones: 0")
  end

end